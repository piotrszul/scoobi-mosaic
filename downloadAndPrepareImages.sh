#!/bin/bash
rm -rf target/sample-urls
rm -rf target/sample-raw-images
rm -rf target/sample-images
sbt 'run-main name.pszul.mosaic.input.GetPicassaUrls target/sample-urls'
sbt 'run-main name.pszul.mosaic.input.DownloadImages target/sample-urls target/sample-raw-images'
sbt 'run-main name.pszul.mosaic.input.SplitIntoTiles target/sample-raw-images target/sample-images'


 
