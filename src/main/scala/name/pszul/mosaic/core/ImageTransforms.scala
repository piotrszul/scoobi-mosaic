package name.pszul.mosaic.core
import name.pszul.mosaic.image.ImageUtils._
import com.jhlabs.image.GrayscaleFilter


object ImageTransforms {

    def toGrayScale(image: Image): Image = {
        return apply(new GrayscaleFilter())(image)
    }

    def toScale(scaledSize: Int)(image: Image): Image = {
        return scaleImage(image, scaledSize, scaledSize)
    }

    def splitAndProcess(splitSize: Int)(f: Image => Image)(image: Image): Seq[Image] = {
        val xtiles = image.getWidth() / splitSize
        val tiles = for (x <- (0 until xtiles); y <- (0 to image.getHeight() / splitSize - 1)) yield (x, y)
        return tiles.map(tile => f(subimage(image, tile._1, tile._2, splitSize)));
    }

}