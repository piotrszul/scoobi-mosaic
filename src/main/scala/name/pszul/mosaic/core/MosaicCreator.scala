package name.pszul.mosaic.core
import name.pszul.mosaic.image.ImageUtils._
import com.nicta.scoobi.core.DList
import com.jhlabs.image.GrayscaleFilter
import java.awt.Graphics
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import com.nicta.scoobi.lib.Relational.join
import com.nicta.scoobi.Scoobi._

class MosaicCreator(cellSize: Int, tileSize: Int, computeSize: Int, implicit val conf: ScoobiConfiguration) {

    def renderPicture(input: DList[(Int, RawImage)], width: Int, height: Int, tileSize: Int): Image = {
        val img: Image = new Image(width * tileSize,
            height * tileSize, BufferedImage.TYPE_INT_RGB);
        val g2: Graphics = img.getGraphics();
        persist(input.materialise).foreach {
            case (i, image) => g2.drawImage(scaleImage((imageFrom(image)), tileSize, tileSize), (i % width) * tileSize, (i / width) * tileSize, null)
        }
        return img
    }

    def distanceToCells(inputCellIndex: Seq[(Int, Image)])(distance: ImageDistance)(x: (String, RawImage)): Seq[(Int, (Double, String))] = x match {
        case (imageId: String, rawImage: RawImage) => {
            val scaledInput: Image = scaleImage(imageFrom(rawImage), computeSize, computeSize)
            return inputCellIndex.map { case (pos, image) => (pos, (distance(image, scaledInput), imageId)) }
        }
    }

    def createMosaic(inputImage: Image, imageDatabase: DList[(String, RawImage)], distance: ImageDistance): Image = {

        val xCells = inputImage.getWidth() / cellSize;
        val yCells = inputImage.getHeight() / cellSize;

        val inputCellIndex: Seq[(Int, Image)] = for (x <- (0 until xCells); y <- (0 until yCells)) 
                yield (x + y * xCells, scaleImage(subimage(inputImage, x, y, cellSize), computeSize, computeSize)) 

        val bestImages: DList[(String, Int)] =
            imageDatabase
                .flatMap(distanceToCells(inputCellIndex)(distance))
                .groupByKey
                .combine((x: (Double, String), y: (Double, String)) => if (x._1 < y._1) x else y)
                .map { case (cellIndex, (distance, imgId)) => (imgId, cellIndex) }

        val output: DList[(Int, RawImage)] = join(bestImages, imageDatabase).map { case (imgId, (cellIndex, img)) => (cellIndex, img) };

        return renderPicture(output, xCells, yCells, tileSize);
    }
}