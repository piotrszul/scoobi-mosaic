package name.pszul.mosaic.input;
import java.awt.image.BufferedImage

import com.nicta.scoobi.Scoobi.DList
import com.nicta.scoobi.Scoobi.ScoobiApp
import com.nicta.scoobi.Scoobi.StringFmt
import com.nicta.scoobi.Scoobi.TraversableFmt
import com.nicta.scoobi.Scoobi.Tuple2Fmt
import com.nicta.scoobi.Scoobi.convertFromSequenceFile
import com.nicta.scoobi.Scoobi.convertToSequenceFile
import com.nicta.scoobi.Scoobi.persist
import com.nicta.scoobi.application.ScoobiConfiguration.toConfiguration

import name.pszul.mosaic.core.ImageTransforms.splitAndProcess
import name.pszul.mosaic.core.ImageTransforms.toScale
import name.pszul.mosaic.image.ImageUtils.Image
import name.pszul.mosaic.image.ImageUtils.RawImage
import name.pszul.mosaic.image.ImageUtils.imageFrom
import name.pszul.mosaic.image.ImageUtils.imageTo
import name.pszul.mosaic.image.ImageUtils.isValidRGBImage
import name.pszul.mosaic.scoobi.ScoobiFix

object SplitIntoTiles extends ScoobiApp {

    def splitImage(f: Image => Seq[Image])(x: (String, RawImage)): Seq[(String, RawImage)] = x match {
        case (imageId, image) => {
            val src: BufferedImage = imageFrom(image)
            val converted = List(src).flatMap(f)
            return converted.zipWithIndex.map { case (img, index) => (imageId + "_" + index, imageTo(img)) }
        }
    }

    def run() {

        ScoobiFix.fixClassPath(configuration)

        val splitSize = 300;
        val tileSize = 75;

        val pathToInputImages: String = if (args.length > 0) args(0) else "target/img-input-raw"
        val pathToOutputImages: String = if (args.length > 1) args(1) else "target/img-input"

        val rawImageDatabase: DList[(String, RawImage)] =
            convertFromSequenceFile(pathToInputImages)

        val imageDatabase = rawImageDatabase
            .filter { case (name, image) => isValidRGBImage(image) }
            .flatMap(splitImage(splitAndProcess(splitSize)(toScale(tileSize))))
        persist(convertToSequenceFile(imageDatabase, pathToOutputImages))
    }

}

