package name.pszul.mosaic.input

import java.io.File
import java.net.URL
import scala.sys.process.urlToProcess
import com.nicta.scoobi.application.ScoobiApp
import com.nicta.scoobi.core.DList
import org.apache.commons.io.output.ByteArrayOutputStream
import com.nicta.scoobi.Scoobi.convertToSequenceFile
import com.nicta.scoobi.Scoobi.convertValueFromSequenceFile
import com.nicta.scoobi.Scoobi.toTextFile
import com.nicta.scoobi.Scoobi.persist
import java.io.IOException
import name.pszul.mosaic.scoobi.ScoobiFix

/**
 * Generates a set of image URLs from the Picassa 'featured' RSS feed.
 * Saves the URLs as a text file - one url per line.
 * 
 * GetPicassaUrls <output-path>
 */
object GetPicassaUrls extends ScoobiApp {

    def run() {

        ScoobiFix.fixClassPath(configuration)    

        def mapPage(i:Int):List[String] = {
            try {
                val maxSize = 500;
                val startIndex = i*maxSize + 1
                val musicElem = scala.xml.XML.load("https://picasaweb.google.com/data/feed/base/featured"
                        +"?alt=rss&kind=photo&access=public&slabel=featured&imgmax=1600&start-index=" + startIndex
                        +"&max-results=" + maxSize)
                val images:List[String] = (musicElem \\ "content" ).map(link => (link \ "@url").text).toList
                return images;
            } catch {
                case ex:IOException => return List.empty
            }
        }
        
        val outputDirectory: String = if (args.length > 0) args(0) else "target/url-input";
        
        val urls: DList[String] = DList.apply((0 until 1)).flatMap(mapPage)
        persist(toTextFile(urls, outputDirectory))
    }	
	
}