package name.pszul.mosaic.input

import java.io.File
import java.net.URL
import scala.sys.process.urlToProcess
import com.nicta.scoobi.application.ScoobiApp
import com.nicta.scoobi.core.DList
import org.apache.commons.io.output.ByteArrayOutputStream
import com.nicta.scoobi.Scoobi.convertToSequenceFile
import com.nicta.scoobi.Scoobi.convertValueFromSequenceFile
import com.nicta.scoobi.Scoobi.toTextFile
import com.nicta.scoobi.Scoobi.persist
import com.nicta.scoobi.Scoobi
import java.io.IOException
import java.io.FileNotFoundException
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.GetMethod
import scala.io.Source
import org.apache.commons.io.IOUtils
import name.pszul.mosaic.scoobi.ScoobiFix

/**
 * Downloads images from the  a text input file with the image URLs.
 * One url per line, e.g:
 * https://lh3.googleusercontent.com/-Y2aT9psinWI/UPtLH3BwEPI/AAAAAAAAArI/sPjgsZyx8-0/s1600/National-Gallery-1_DSF2236.jpg
 * https://lh5.googleusercontent.com/-s104F4huvPE/UJlbSMwri2I/AAAAAAAAC9w/Oapa3qUSqHk/s1600/art_horse_120524.jpg
 * https://lh5.googleusercontent.com/-qrOdneOszGs/Tjix33OdWbI/AAAAAAAABPU/tZ924zLc9Dg/s1600/_MG_0438.jpg
 * ....
 * 
 * DownloadImages <input-path> <output-path>
 * 
 */
object DownloadImages extends ScoobiApp {

    def run() {

        ScoobiFix.fixClassPath(configuration)

        // this is perhaps a beter version as it uses connection keep alive 
        // so should be better for retrieving lots of small images
        val httpclient: HttpClient = new HttpClient();
        
        def downloadUrl(url: String): List[Byte] = {
            val httpget = new GetMethod(url);
            httpget.setFollowRedirects(false)
            try {
                httpclient.executeMethod(httpget);
                if (httpget.getStatusCode() == 200) {
                    val bufferSteam = new ByteArrayOutputStream();
                    IOUtils.copy(httpget.getResponseBodyAsStream(), bufferSteam)
                    return bufferSteam.toByteArray().toList
                } else {
                    return List.empty
                }
            } catch {
                case ex: Exception => {
                    return List.empty
                }
            } finally {
                httpget.releaseConnection();
            }
        }

        val inputDirectory: String = if (args.length > 0) args(0) else "target/url-input";
        val outputDirectory: String = if (args.length > 1) args(1) else "target/img-input-raw";

        val urls: DList[String] = Scoobi.fromTextFile(inputDirectory)
        val output = urls.map { case url => (url, downloadUrl(url)) }.filter { case (url, image) => !image.isEmpty }
        persist(convertToSequenceFile(output, outputDirectory))
    }

}