package name.pszul.mosaic.scoobi
import org.apache.hadoop.conf.Configuration

object ScoobiFix {
    
    def fixClassPath(conf: Configuration) = {
        val classPath: String = conf.get("mapred.classpath")
        val classPathFiltered = classPath.split(":").filter { path => !(path.contains("cdh4") && path.contains("hadoop")) }
        conf.set("mapred.job.classpath.files", classPathFiltered.mkString(":"));

    }
}