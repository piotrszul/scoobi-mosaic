package name.pszul.mosaic.app;
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path

import com.nicta.scoobi.Scoobi.DList
import com.nicta.scoobi.Scoobi.ScoobiApp
import com.nicta.scoobi.Scoobi.TraversableFmt
import com.nicta.scoobi.Scoobi.convertFromSequenceFile
import com.nicta.scoobi.application.ScoobiConfiguration.toConfiguration

import javax.imageio.ImageIO
import name.pszul.mosaic.core.MosaicCreator
import name.pszul.mosaic.image.ImageUtils.RawImage
import name.pszul.mosaic.image.ImageUtils.distanceRGB
import name.pszul.mosaic.image.ImageUtils.isValidRGBImage
import name.pszul.mosaic.scoobi.ScoobiFix

object CreateMosaicApp extends ScoobiApp {

    def run() {

        ScoobiFix.fixClassPath(configuration)
        val fileSystem = FileSystem.get(configuration)

        val pathToInputImage: String = if (args.length > 0) args(0) else "src/test/resources/dove.jpg"
        val pathToImageDatabase: String = if (args.length > 1) args(1) else "target/img-input"
        val pathToOutputImage: String = if (args.length > 2) args(2) else "target/mosaic-output.jpg"
        val cellSize = 30;
        val tileSize = 50;
        val computeSize = 10;
        
        println("Running mosaic:") 
        println("pathToInputImage: " + pathToInputImage)
        println("pathToImageDatabase: " + pathToImageDatabase)
        println("pathToOutputImage: " +  pathToOutputImage)
        println("cellSize: " + cellSize)
        println("tileSize: "  + tileSize)
        println("computeSize: " + computeSize)
        

        val inputImage = ImageIO.read(fileSystem.open(new Path(pathToInputImage)))

        val rawImageDatabase: DList[(String, RawImage)] =
            convertFromSequenceFile(pathToImageDatabase)

        val imageDatabase = rawImageDatabase
            .filter { case (name, image) => isValidRGBImage(image) }

        val mosaicCreator: MosaicCreator = new MosaicCreator(cellSize, tileSize, computeSize, configuration)
        val img = mosaicCreator.createMosaic(inputImage, imageDatabase, distanceRGB)

        ImageIO.write(img, "JPEG", fileSystem.create(new Path(pathToOutputImage)))

    }

}

